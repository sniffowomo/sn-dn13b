<h1 align="center"><code> 🌐:SN-DN13B </code></h1>
<h2 align="center"><i> NextJs13 Study Learn  </i></h2>

---

1. [What ?](#what-)
   1. [Main Reference](#main-reference)
   2. [NextJs Resource Github](#nextjs-resource-github)
   3. [Regarding Installation](#regarding-installation)
2. [Dirs](#dirs)
3. [Branches](#branches)

---

# What ?

1. Main learning repo for NextJS and related
2. Note this repo is fork of - [`https://github.com/sniffowomo/sn-DN13B`](https://github.com/sniffowomo/sn-DN13B)
   1. Stopped working on this since typescript language server keeps crashing, so now going to test it out here and see if that is the case, if not then most if not all the nextjs related dev will be done here
   2. Note other work in other repo , not have same problem , so can be issue with the version of codepscae which is running.
   3. Pain in the azs to try to isolate the issue
   4. Note first install of `cargo` stuff takes more than half hour, then you cna only start the actual work

## Main Reference

The following is the main course reference

[Dav - NextJs Beginner Tutorial Playlist](https://www.youtube.com/playlist?list=PL0Zuz27SZ-6Pk-QJIdGd1tGZEzy9RTgtj)

- The best playlist that talks about using nextjs
- THis repo will also have other related stuff which needs to be learnt
- Each one will be in its own branch
- Also for testing it will be hosted on vercel
- Braches dont have sharp delienation

## NextJs Resource Github

1. This is the main github which has the tut files last time when attempting this you have problems so keeping this reference here now

[`https://github.com/gitdagray/next-js-course`](https://github.com/gitdagray/next-js-course)

- This has been downloaded here

## Regarding Installation

According to the [docs](https://beta.nextjs.org/docs/installation), instal can be done like this

```sh
npx create-next-app@latest --experimental-app
```

- Note it will ask if you want tailwind css also while install
- Note the new version of this has not number 13 in the default page
- you can use `pnpx` like so

```ml
pnpx create-next-app@latest --experimental-app
```

- has the same installaltion

# Dirs

|       N       |              ?              |
| :-----------: | :-------------------------: |
| [`nj`](./nj/) | Downloaded course materials |

# Branches

> Work isolation in branches

```sh
❯ gb
* main
  n5
  nj1
  remotes/origin/HEAD -> origin/main
  remotes/origin/main
  remotes/origin/n5
  remotes/origin/nj1

```

- These are the current branches

|  N   |                 ?                 |
| :--: | :-------------------------------: |
| `n5` | Build a small project with nextjs |
